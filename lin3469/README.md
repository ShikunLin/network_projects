# Proxy Server, Web Server And DNS Server  

Author: Shikun Lin  
Email: shikunl@163.com  

## Project Description  

This is project contian three programs. There has three servers, such as proxy server, web server and DNS server. All of them are written in python. If you want to use these three program together, you may need change the domain name on web_server.py.  

#### Proxy Server  

Proxy server which can handle an http request for users. It Runs on 8888 port. It can recieve a http request from an user. If the file whcih user require is on the caches. Then, the proxy server would send the file form the cache to user. If the file is not in the cache, the proxy server will send the request to the remote server to obtian the file. Then, save the file into caches.   
Following Websites are tested and works well:  
1. www.ebay.com  
2. www.facebook.com  
3. www.youtube.com  
4. www.yahoo.com  
5. www.nordstrom.com  

#### Web Server  

This is a simple web server. It runs on 80 port. It can send files to users. The files are located ./html directory. If the file not exist in ./html, the server would send the 404 to user.  

#### DNS Server

This is a simple DNS server. It runs on 53 port. After the user set the DNS server on their machine. It can point all domain names to localhost "127.0.0.1".  

## How to RUN  
This insruction can tell you how to run you project on your machine. This project is tested on linux and Mac OS. It should be run well on UNIX-like operating systems. Not sure on Windows machine.

### Prerequisite  

It needs [python2](https://www.python.org/). Not python3.  

#### How to Run Proxy Server  
You can find the using examples and details on report.pdf  

1. Open the terminal or command line.  
2. cd to the directory which contain "./caches" and "proxy_server.py"   
3. Run "python proxy_server.py 127.0.0.1"  
4. Then, open the browser.  
5. If you did not run DNS server, input URL like "127.0.0.1:8888/www.ebay.com" on browser. If you did run DNS server, you can input "127.0.0.1:8888/www.ebay.com" or "www.lin.432:8888/www.ebay.com"

#### How to Run Web Server  

You can find the using examples and details on report.pdf  

1. Open the terminal or command line.  
2. cd to the directory which contain "./html" and "web_server.py"  
3. Run "sudo python web_server.py". Because of it runs on port 80, you need root to run it.  
4.  Then, open the browser.  
5. If you did not run the DNS server, input "127.0.0.1/test.html". If you did run the DNS server, you can input "www.lin.432/test.html".
 
#### How to Run DNS Server  
You can find the using examples and details on report.pdf  

1. Open the terminal or command line.  
2. cd to the directory which contain "dns_server.py"  
3. Run "sudo python dns_server.py". Because of it runs on port 53, you need root to run it.  
4. Set the DNS server ip address to 127.0.0.1 on you machine. [Here](https://www.expressvpn.com/support/troubleshooting/set-dns-servers-for-mac-os/) is a tutorial for MAC OS.  

#### How to Use All Server  

1. Run all three servers 
2. Open browser, input "127.0.0.1:8888/www.lin.432/emoji.jpg" or "www.lin.432:8888/www.lin.432/emoji.jpg".

## How to Use and Test  

Please read report.pdf. It contians all details.

## Acknowledgement  
1. James Routley, [Let's hand write DNS messages](https://routley.io/tech/2017/12/28/hand-writing-dns-messages.html)  
2. [Python socket documents](https://docs.python.org/2/library/socket.html)

