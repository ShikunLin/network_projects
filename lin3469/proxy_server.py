'''
Author: Shikun Lin

CIS432 UO 18F

This is an web proxy sercer which only accpet HTTP request.
It might not handle HTTPS request.

It can caches the web objects into ./caches directory


'''


import socket
import sys


recv_buffer_size = 4096



def main():

	#require a ip address for the proxy server, if the user did not
	#input any ip address. Then, print warning message
	if len(sys.argv) <= 1:
		print ('Usage : "python proxy_server.py server_ip"\n'\
			+ '[server_ip : It is the IP Address Of Proxy Server]')
		sys.exit(2)

	server_host = sys.argv[1]
	port = 8888


	#Try to bind the ip address with port 8888. If bind failed, then 
	#print out the error message
	try:
		server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		server_socket.bind((server_host,port))
		server_socket.listen(25)

	except socket.error as e:
		print(e)


	#Using a while loop make the server always wait for a connect
	while True:
		print("Ready to serve...")
		#accpet connection from the user or browser
		client_socket, addr = server_socket.accept()
		print("Received a connection from: IP:"+str(addr[0])+" port: "+str(addr[1]))

		#Get the request message from the socket
		message = client_socket.recv(recv_buffer_size)
		print(message)

		#Get the file name from the http request
		filename = message.split()[1].partition("/")[2]
		filetouse = filename.split('/')[-1]
		print(filename)
		print(filetouse)
		fileExist = False

		#Try to cache the file from the ./caches
		try:
			#If the file exist, then send the file to user client
			f = open("./caches/"+filename,"r")
			outputdata = f.readlines()
			fileExist = True
			client_socket.sendall(''.join(outputdata))
			print("READ from cache")

		#If file not exist 
		except IOError:

			#Create an socket which to obtian the web content form 
			#remote server
			client_get = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			#Get the remote server host name
			hostn = filename.replace("www.","",1)
			print(hostn)

			if not fileExist:
				#Try to connect to the remote server
				try:

					client_get.connect((hostn,80))

					fileobj = client_get.makefile('wr',0)
					fileobj.write(("GET "+ "http://" + filename + "/ HTTP/1.1\n\n"))

					#send the request to the remote server
					client_get.send(message)


					#create the cache file
					tmpFile = open("./caches/"+filetouse,"w")

					#Using while loop to obtian all data from remote server
					while 1:
						#Set up the timer for the recv funtion
						client_get.settimeout(5.0)
						data = client_get.recv(recv_buffer_size)
						client_get.settimeout(None)
						#If the data is empty break the loop
						if not data: break
						#Write the data to caches file
						tmpFile.write(data)
						#Send the data to the user client
						client_socket.sendall(data)


					tmpFile.close()
					fileobj.close()

					print("SENT!!!!!!!!!!!!!!")

				#If connect remote server failed, then print 
				#the error message
				except socket.error as e:
					print("Illegal Request",e)

			else:
				pass
			client_socket.close()
			client_get.close()



if __name__ == '__main__':
	main()
