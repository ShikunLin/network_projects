
'''
Author: Shikun Lin
CIS432 UO 18F

This is an DNS server which can point all domains to 
localhost

'''
import socket
import sys


recv_buffer_size = 1024
#holder for IP address
host_ip = "127.0.0.1"




#This is an helper function can convert a IP address
# into bytes
def get_bytes_for_ip():
    ip_byte = ''
    ip_lsit = host_ip.split('.')
    for i in ip_lsit:
        ip_byte += chr(int(i))
    return ip_byte

    

'''
This is an response format
+---------------------+
|        Header       |
+---------------------+
|       Question      | the question for the name server
+---------------------+
|        Answer       | Resource Records (RRs) answering the question
+---------------------+
|      Authority      | RRs pointing toward an authority
+---------------------+
|      Additional     | RRs holding additional information
+---------------------+
'''

#Build the DNS response in rcf format
def build_response(data):
    response = ''
    ip_byte = get_bytes_for_ip()
    response += data[:2] + "\x81\x80" + data[4:6] + data[4:6] + \
    "\x00\x00\x00\x00" + data[12:] + "\xc0\x0c" + \
    "\x00\x01\x00\x01\x00\x00\x00\x3c\x00\x04" + ip_byte

    return response



def main():
    dns_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
    	#Bind the local host and port 53 which is port for 
    	#an DNS server
        dns_server.bind(("",53))

     #If there has an error, then print the error and exit the program
    except socket.error as e:
        print(e)
        sys.exit(2)


    #Using while loop make the server keep running
    while True:
        try:
        	#recv data from port 53.
            data, addr = dns_server.recvfrom(recv_buffer_size)

            #Build the response with the data 
            response = build_response(data)

            #send response
            dns_server.sendto(response,addr)

            print("Response Sent!!!!")

        #If have an error. Then, print the error message 
        # and close the socket
        except:
            print("DNS server closed!")
            dns_server.close()
            break





if __name__ == '__main__':
    main()
