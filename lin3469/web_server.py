'''
Author: Shikun Lin

CIS432 UO 18F

This is a simple web server runing on python socket

This server can send the files which located in ./html 
directory to the users.


'''
import socket
import sys



def main():
    #Set up the domain name host for the project whcih we would 
    #use it with DNS server program 
    host = "www.lin.432"
    #Set the port to the 80 which for http 
    port = 80
    web_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #Try to bind to the the ip address by the domain name 
    try:
        web_server.bind((host,port))
        print("Bind to www.lin.432")

    #If the domian name can not be used. Or If you want to run dns_server
    #and web_server sepreately. Then, using localhost
    except socket.error as e:
        web_server.bind(('127.0.0.1',port))
        print("Bind to 127.0.0.1")


    web_server.listen(25)
    #Print out the bind information
    print("Web Server is started on port: "+ str(port))


    #Using while loop keeo the server running
    while True:

        print("Ready to serve...")
        cli, client_add = web_server.accept()

        #Get the http request from the users
        request = cli.recv(4096)
        #get the file name from request
        filename = request.split()[1].partition("/")[2]

        #We nned to Check the user using this with dns_server or not
        #to make the filetouse
        if "www." in filename:
            filetouse = filename.split('/')[-2]
        else:
            filetouse = filename.split('/')[-1]
        print("filetouse === ",filetouse)

        #Make the header for the http response
        header = "HTTP/1.1 200 OK\r\n\r\n"
        header_not_found = "HTTP/1.1 404\r\n\r\n"

        #Try to open the files from ./html
        try:
            f = open("./html/" +filetouse, 'r')
            #read the files
            data = f.readlines()
            out = ''.join(data)

            #Make the data with header
            out = header + out

            #Send the data to the users
            cli.sendall(out)
            print("File has been sent!!!!!!")

        #If the socket error print the error message
        except socket.error as e:
            print(e)

        #If the file not exist in ./html thensend an 404, and 
        #information for the users
        except IOError:
            out = "The file: " + filetouse + " is not in server!!! Please try other file!"
            out = header_not_found + out
            print(filetouse + " is not in server!")
            cli.sendall(out)


        cli.close()

if __name__ == '__main__':
	main()
